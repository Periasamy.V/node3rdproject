import { Component, Input, Output, EventEmitter } from '@angular/core';

interface userInput {
  name: string;
  age: string;
  id: number;
}

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css'],
})
export class ChildComponent {
  @Input() user: userInput;
  @Output() userEvent = new EventEmitter<userInput>();

  constructor() {
    this.user = {} as userInput;
  }
  sendUserEvent(): void {
    this.userEvent.emit(this.user);
  }
}
